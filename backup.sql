CREATE TABLE banco (
                                id                   bigint  NOT NULL  AUTO_INCREMENT  PRIMARY KEY,
                                nombre               varchar(255)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

CREATE TABLE contrato_laboral (
                                           id                   bigint  NOT NULL  AUTO_INCREMENT  PRIMARY KEY,
                                           sueldo_base          double      ,
                                           tipo_contrato        double
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

CREATE TABLE cuenta_bancaria (
                                          id                   bigint  NOT NULL  AUTO_INCREMENT  PRIMARY KEY,
                                          nummero_cuenta       varchar(255)      ,
                                          id_banco             bigint
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

CREATE TABLE empleado (
                                   id                   bigint  NOT NULL  AUTO_INCREMENT  PRIMARY KEY,
                                   apellidos            varchar(255)      ,
                                   nombres              varchar(255)      ,
                                   id_contrato          bigint      ,
                                   id_cuenta            bigint
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

CREATE TABLE rol (
                              id                   bigint  NOT NULL  AUTO_INCREMENT  PRIMARY KEY,
                              nombre               varchar(255)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

CREATE TABLE usuario (
                                  id                   bigint  NOT NULL  AUTO_INCREMENT  PRIMARY KEY,
                                  active               boolean  NOT NULL    ,
                                  password             varchar(255)      ,
                                  username             varchar(50)  NOT NULL    ,
                                  empleado_id          bigint      ,
                                  CONSTRAINT `UK_863n1y3x0jalatoir4325ehal` UNIQUE ( username )
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

CREATE TABLE usuario_rol (
                                      usuario_id           bigint  NOT NULL    ,
                                      rol_id               bigint  NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE INDEX `FKetfcfd65f8lkh7ym745mr4e49` ON cuenta_bancaria ( id_banco );

CREATE INDEX `FKnmvnd1rr7top60bqkgii8o1ad` ON empleado ( id_contrato );

CREATE INDEX `FKohyrhnlhi2b1p8cytxdnx170q` ON empleado ( id_cuenta );

CREATE INDEX `FKjn24nnj8w0mmt2eq54ol6xrq2` ON usuario ( empleado_id );

CREATE INDEX `FK610kvhkwcqk2pxeewur4l7bd1` ON usuario_rol ( rol_id );

CREATE INDEX `FKbyfgloj439r9wr9smrms9u33r` ON usuario_rol ( usuario_id );

ALTER TABLE cuenta_bancaria ADD CONSTRAINT `FKetfcfd65f8lkh7ym745mr4e49` FOREIGN KEY ( id_banco ) REFERENCES banco( id ) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE empleado ADD CONSTRAINT `FKnmvnd1rr7top60bqkgii8o1ad` FOREIGN KEY ( id_contrato ) REFERENCES contrato_laboral( id ) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE empleado ADD CONSTRAINT `FKohyrhnlhi2b1p8cytxdnx170q` FOREIGN KEY ( id_cuenta ) REFERENCES cuenta_bancaria( id ) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE usuario ADD CONSTRAINT `FKjn24nnj8w0mmt2eq54ol6xrq2` FOREIGN KEY ( empleado_id ) REFERENCES empleado( id ) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE usuario_rol ADD CONSTRAINT `FK610kvhkwcqk2pxeewur4l7bd1` FOREIGN KEY ( rol_id ) REFERENCES rol( id ) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE usuario_rol ADD CONSTRAINT `FKbyfgloj439r9wr9smrms9u33r` FOREIGN KEY ( usuario_id ) REFERENCES usuario( id ) ON DELETE NO ACTION ON UPDATE NO ACTION;

INSERT INTO banco( id, nombre ) VALUES ( 1, 'silviopd1110');
INSERT INTO banco( id, nombre ) VALUES ( 4, null);
INSERT INTO banco( id, nombre ) VALUES ( 5, 'silviopd2');
INSERT INTO contrato_laboral( id, sueldo_base, tipo_contrato ) VALUES ( 1, 30000.0, 0.0);
INSERT INTO contrato_laboral( id, sueldo_base, tipo_contrato ) VALUES ( 2, 300.0, 0.0);
INSERT INTO contrato_laboral( id, sueldo_base, tipo_contrato ) VALUES ( 4, 300.0, 0.0);
INSERT INTO contrato_laboral( id, sueldo_base, tipo_contrato ) VALUES ( 5, 30000.0, 1.0);
INSERT INTO cuenta_bancaria( id, nummero_cuenta, id_banco ) VALUES ( 1, null, 4);
INSERT INTO cuenta_bancaria( id, nummero_cuenta, id_banco ) VALUES ( 3, null, 4);
INSERT INTO cuenta_bancaria( id, nummero_cuenta, id_banco ) VALUES ( 4, 'silviopd0', null);
INSERT INTO empleado( id, apellidos, nombres, id_contrato, id_cuenta ) VALUES ( 1, 'Apellidos 1', 'Empleado 1', null, null);
INSERT INTO empleado( id, apellidos, nombres, id_contrato, id_cuenta ) VALUES ( 2, 'Apellidosasdasd 3', 'silviopdasd', 1, 1);
INSERT INTO empleado( id, apellidos, nombres, id_contrato, id_cuenta ) VALUES ( 3, 'Apellidos 3', 'silviopd', null, null);
INSERT INTO rol( id, nombre ) VALUES ( 1, 'Secretaria mod');
INSERT INTO rol( id, nombre ) VALUES ( 2, 'Catedratico2');
INSERT INTO rol( id, nombre ) VALUES ( 3, 'Catedratico3');
INSERT INTO usuario( id, active, password, username, empleado_id ) VALUES ( 1, 0, '12311145', 'empleado1115', 2);
INSERT INTO usuario( id, active, password, username, empleado_id ) VALUES ( 2, 0, '12145', 'empleaas777', 1);
INSERT INTO usuario( id, active, password, username, empleado_id ) VALUES ( 5, 0, '12345', 'empleado4', null);
INSERT INTO usuario( id, active, password, username, empleado_id ) VALUES ( 6, 0, '12145', 'empleaassss7', null);
INSERT INTO usuario_rol( usuario_id, rol_id ) VALUES ( 1, 1);
INSERT INTO usuario_rol( usuario_id, rol_id ) VALUES ( 6, 2);
INSERT INTO usuario_rol( usuario_id, rol_id ) VALUES ( 6, 3);