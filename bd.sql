CREATE SCHEMA examenfinalsilviopd;

CREATE TABLE examenfinalsilviopd.movie ( 
	id                   bigint  NOT NULL  AUTO_INCREMENT  PRIMARY KEY,
	description          varchar(255)      ,
	name                 varchar(255)      ,
	premiere             boolean      ,
	premier_date         datetime      
 ) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8mb4;

CREATE TABLE examenfinalsilviopd.review ( 
	id                   bigint  NOT NULL  AUTO_INCREMENT  PRIMARY KEY,
	comment              varchar(255)      ,
	commentator          varchar(255)      ,
	create_at            datetime      ,
	movie_id             bigint      ,
	CONSTRAINT `FK8378dhlpvq0e9tnkn9mx0r64i` FOREIGN KEY ( movie_id ) REFERENCES examenfinalsilviopd.movie( id ) ON DELETE NO ACTION ON UPDATE NO ACTION
 ) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;

CREATE TABLE examenfinalsilviopd.usuario ( 
	id                   bigint  NOT NULL  AUTO_INCREMENT  PRIMARY KEY,
	password             varchar(255)      ,
	username             varchar(255)      
 ) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

CREATE INDEX `FK8378dhlpvq0e9tnkn9mx0r64i` ON examenfinalsilviopd.review ( movie_id );

INSERT INTO examenfinalsilviopd.movie( id, description, name, premiere, premier_date ) VALUES ( 39, 'asd123', 'Dr. Strange', 1, '2022-05-26 07.00.00 p.m.');
INSERT INTO examenfinalsilviopd.movie( id, description, name, premiere, premier_date ) VALUES ( 46, 'qweqe', 'Dr. Strange 2', 1, '2022-05-23 07.00.00 p.m.');
INSERT INTO examenfinalsilviopd.movie( id, description, name, premiere, premier_date ) VALUES ( 48, 'sdf', 'Dr. Strange 3', 0, '2022-05-17 07.00.00 p.m.');
INSERT INTO examenfinalsilviopd.movie( id, description, name, premiere, premier_date ) VALUES ( 49, 'sd', 'ads', 1, '2022-05-20 07.00.00 p.m.');
INSERT INTO examenfinalsilviopd.review( id, comment, commentator, create_at, movie_id ) VALUES ( 8, 'asd', 'asdsss', '2022-05-17 07.00.00 p.m.', 39);
INSERT INTO examenfinalsilviopd.usuario( id, password, username ) VALUES ( 1, '123', 'silviopd');
INSERT INTO examenfinalsilviopd.usuario( id, password, username ) VALUES ( 2, '1234', 'silviopd2');