package com.example.examen.Service;

import com.example.examen.Entity.Banco;
import com.example.examen.Entity.ContratoLaboral;
import com.example.examen.Entity.CuentaBancaria;
import com.example.examen.Repository.BancoRepository;
import com.example.examen.Repository.CuentaBancariaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CuentaBancariaServiceImpl implements CuentaBancariaService {

    @Autowired
    private CuentaBancariaRepository cuentaBancariaRepository;

    @Autowired
    private BancoRepository bancoRepository;

    @Override
    public List<CuentaBancaria> listar() {
        return cuentaBancariaRepository.findAll();
    }

    @Override
    public CuentaBancaria registrar(CuentaBancaria cuentaBancaria) {
        return cuentaBancariaRepository.save(cuentaBancaria);
    }

    @Override
    public CuentaBancaria modificar(Long id, CuentaBancaria cuentaBancaria) {
        Optional<CuentaBancaria> cuentaBancariaFounded = cuentaBancariaRepository.findById(id);
        if (cuentaBancariaFounded.isPresent()) {
            CuentaBancaria personaUpdate = new CuentaBancaria();
            personaUpdate.setId(id);
            personaUpdate.setNroCuenta(cuentaBancaria.getNroCuenta() != null ? cuentaBancaria.getNroCuenta() : cuentaBancariaFounded.get().getNroCuenta());

            if (cuentaBancaria.getBanco() != null) {
                Optional<Banco> bancoFounded = bancoRepository.findById(cuentaBancaria.getBanco().getId());
                if (bancoFounded.isPresent()) {
                    personaUpdate.setBanco(cuentaBancaria.getBanco() != bancoFounded.get() ? cuentaBancaria.getBanco() : bancoFounded.get());
                }
            }

            return cuentaBancariaRepository.save(personaUpdate);
        }
        return null;
    }

    @Override
    public String eliminar(Long id) {
        try {
            cuentaBancariaRepository.deleteById(id);
            return "Eliminacion Correcta";
        } catch (Exception ex) {
            return "Error durante la eliminacion";
        }
    }
}
