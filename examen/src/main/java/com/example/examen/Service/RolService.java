package com.example.examen.Service;

import com.example.examen.Entity.Rol;

import java.util.List;

public interface RolService {
    //CRUD
    public List<Rol> listar();

    public Rol registrar(Rol rol);

    public Rol modificar(Long id, Rol rol);

    public String eliminar(Long id);
}
