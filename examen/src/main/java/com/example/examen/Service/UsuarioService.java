package com.example.examen.Service;

import com.example.examen.Entity.Empleado;
import com.example.examen.Entity.Usuario;

import java.util.List;

public interface UsuarioService {

    //CRUD
    public List<Usuario> listar();

    public Usuario registrar(Usuario usuario);

    public Usuario modificar(Long id, Usuario usuario);

    public String eliminar(Long id);

    public List<Usuario> listaEmpleadosActivos();
}
