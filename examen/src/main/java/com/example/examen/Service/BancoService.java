package com.example.examen.Service;

import com.example.examen.Entity.Banco;

import java.util.List;

public interface BancoService {

    //CRUD
    public List<Banco> listar();

    public Banco registrar(Banco banco);

    public Banco modificar(Long id, Banco banco);

    public String eliminar(Long id);
}
