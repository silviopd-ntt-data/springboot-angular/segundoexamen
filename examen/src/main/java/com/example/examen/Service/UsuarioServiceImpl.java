package com.example.examen.Service;

import com.example.examen.Entity.Empleado;
import com.example.examen.Entity.Rol;
import com.example.examen.Entity.Usuario;
import com.example.examen.Repository.RolRepository;
import com.example.examen.Repository.UsuarioRepository;
import com.example.examen.Repository.EmpleadoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UsuarioServiceImpl implements UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private EmpleadoRepository empleadoRepository;

    @Autowired
    private RolRepository rolRepository;

    @Override
    public List<Usuario> listar() {
        return usuarioRepository.findAll();
    }

    @Override
    public Usuario registrar(Usuario usuario) {
        return usuarioRepository.save(usuario);
    }

    @Override
    public Usuario modificar(Long id, Usuario usuario) {

        Optional<Usuario> usuarioFounded = usuarioRepository.findById(id);
        if (usuarioFounded.isPresent()) {
            Usuario personaUpdate = new Usuario();
            personaUpdate.setId(id);
            personaUpdate.setUsername(usuario.getUsername() != null ? usuario.getUsername() : usuarioFounded.get().getUsername());
            personaUpdate.setPassword(usuario.getPassword() != null ? usuario.getPassword() : usuarioFounded.get().getPassword());
            personaUpdate.setActive(usuario.isActive() != usuarioFounded.get().isActive() ? usuario.isActive() : usuarioFounded.get().isActive());

            if (usuario.getEmpleado() != null) {
                Optional<Empleado> empleadoFounded = empleadoRepository.findById(usuario.getEmpleado().getId());
                if (empleadoFounded.isPresent() && empleadoFounded.get().getId() != null) {
                    personaUpdate.setEmpleado(empleadoFounded.get());
                }
            }

            if (!usuario.getRolList().isEmpty()) {
                List<Rol> rolList = new ArrayList<>();
                for (Rol rol : usuario.getRolList()) {
                    Optional<Rol> rolFounded = rolRepository.findById(rol.getId());
                    if (rolFounded.isPresent() && rolFounded.get().getId() != null) {
                        rolList.add(rolFounded.get());
                    }

                }
                if (rolList.size() > 0) {
                    personaUpdate.setRolList(rolList);
                }
            }

            return usuarioRepository.save(personaUpdate);
        }
        return null;
    }

    @Override
    public String eliminar(Long id) {
        try {
            usuarioRepository.deleteById(id);
            return "Eliminacion Correcta";
        } catch (Exception ex) {
            return "Error durante la eliminacion";
        }
    }

    @Override
    public List<Usuario> listaEmpleadosActivos() {
        return usuarioRepository.listarUsuarioActivos();
    }
}
