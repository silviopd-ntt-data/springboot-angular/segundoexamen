package com.example.examen.Service;

import com.example.examen.Entity.ContratoLaboral;
import com.example.examen.Entity.Empleado;

import java.util.List;

public interface EmpleadoService {

    //CRUD
    public List<Empleado> listar();

    public Empleado registrar(Empleado empleado);

    public Empleado modificar(Long id, Empleado empleado);

    public String eliminar(Long id);

    public List<Empleado> buscarEmpleado(String texto);
}
