package com.example.examen.Service;

import com.example.examen.Entity.Banco;
import com.example.examen.Entity.Rol;
import com.example.examen.Repository.RolRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RolServiceImpl implements RolService {

    @Autowired
    private RolRepository rolRepository;

    @Override
    public List<Rol> listar() {
        return rolRepository.findAll();
    }

    @Override
    public Rol registrar(Rol rol) {
        return rolRepository.save(rol);
    }

    @Override
    public Rol modificar(Long id, Rol rol) {
        Optional<Rol> rolFounded = rolRepository.findById(id);
        if (rolFounded.isPresent()) {
            Rol personaUpdate = new Rol();
            personaUpdate.setId(id);
            personaUpdate.setNombre(rol.getNombre() != null ? rol.getNombre() : rolFounded.get().getNombre());
            return rolRepository.save(personaUpdate);
        }
        return null;
    }

    @Override
    public String eliminar(Long id) {
        try {
            rolRepository.deleteById(id);
            return "Eliminacion Correcta";
        } catch (Exception ex) {
            return "Error durante la eliminacion";
        }
    }
}
