package com.example.examen.Service;

import com.example.examen.Entity.ContratoLaboral;
import com.example.examen.Entity.CuentaBancaria;

import java.util.List;

public interface CuentaBancariaService {

    //CRUD
    public List<CuentaBancaria> listar();

    public CuentaBancaria registrar(CuentaBancaria cuentaBancaria);

    public CuentaBancaria modificar(Long id, CuentaBancaria cuentaBancaria);

    public String eliminar(Long id);
}
