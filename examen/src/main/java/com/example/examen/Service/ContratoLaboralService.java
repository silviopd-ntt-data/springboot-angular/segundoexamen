package com.example.examen.Service;

import com.example.examen.Entity.Banco;
import com.example.examen.Entity.ContratoLaboral;

import java.util.List;

public interface ContratoLaboralService {

    //CRUD
    public List<ContratoLaboral> listar();

    public ContratoLaboral registrar(ContratoLaboral contratoLaboral);

    public ContratoLaboral modificar(Long id, ContratoLaboral contratoLaboral);

    public String eliminar(Long id);
}
