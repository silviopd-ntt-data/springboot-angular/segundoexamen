package com.example.examen.Service;

import com.example.examen.Entity.Banco;
import com.example.examen.Entity.ContratoLaboral;
import com.example.examen.Repository.ContratoLaboralRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ContratoLaboralServiceImpl implements ContratoLaboralService {

    @Autowired
    private ContratoLaboralRepository contratoLaboralRepository;

    @Override
    public List<ContratoLaboral> listar() {
        return contratoLaboralRepository.findAll();
    }

    @Override
    public ContratoLaboral registrar(ContratoLaboral contratoLaboral) {
        return contratoLaboralRepository.save(contratoLaboral);
    }

    @Override
    public ContratoLaboral modificar(Long id, ContratoLaboral contratoLaboral) {
        Optional<ContratoLaboral> contratoLaboralFounded = contratoLaboralRepository.findById(id);
        if (contratoLaboralFounded.isPresent()) {
            ContratoLaboral personaUpdate = new ContratoLaboral();
            personaUpdate.setId(id);
            personaUpdate.setSueldoBase(contratoLaboral.getSueldoBase() != 0 ? contratoLaboral.getSueldoBase() : contratoLaboralFounded.get().getSueldoBase());
            personaUpdate.setTipoContrato(contratoLaboral.getTipoContrato() != 0 ? contratoLaboral.getTipoContrato() : contratoLaboralFounded.get().getTipoContrato());
            return contratoLaboralRepository.save(personaUpdate);
        }
        return null;
    }

    @Override
    public String eliminar(Long id) {
        try {
            contratoLaboralRepository.deleteById(id);
            return "Eliminacion Correcta";
        } catch (Exception ex) {
            return "Error durante la eliminacion";
        }
    }
}
