package com.example.examen.Service;

import com.example.examen.Entity.ContratoLaboral;
import com.example.examen.Entity.CuentaBancaria;
import com.example.examen.Entity.Empleado;
import com.example.examen.Repository.ContratoLaboralRepository;
import com.example.examen.Repository.CuentaBancariaRepository;
import com.example.examen.Repository.EmpleadoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EmpleadoServiceImpl implements EmpleadoService {

    @Autowired
    private EmpleadoRepository empleadoRepository;

    @Autowired
    private CuentaBancariaRepository cuentaBancariaRepository;

    @Autowired
    private ContratoLaboralRepository contratoLaboralRepository;

    @Override
    public List<Empleado> listar() {
        return empleadoRepository.findAll();
    }

    @Override
    public Empleado registrar(Empleado empleado) {
        return empleadoRepository.save(empleado);
    }

    @Override
    public Empleado modificar(Long id, Empleado empleado) {
        Optional<Empleado> empleadoFounded = empleadoRepository.findById(id);
        if (empleadoFounded.isPresent()) {
            Empleado personaUpdate = new Empleado();
            personaUpdate.setId(id);
            personaUpdate.setNombres(empleado.getNombres() != null ? empleado.getNombres() : empleadoFounded.get().getNombres());
            personaUpdate.setApellidos(empleado.getApellidos() != null ? empleado.getApellidos() : empleadoFounded.get().getApellidos());

            Optional<CuentaBancaria> cuentaBancariaFounded = cuentaBancariaRepository.findById(empleado.getCuentaBancaria().getId());
            if (cuentaBancariaFounded.isPresent() && empleado.getCuentaBancaria() != null) {
                personaUpdate.setCuentaBancaria(cuentaBancariaFounded.get());
            }

            Optional<ContratoLaboral> contratoLaboralFounded = contratoLaboralRepository.findById(empleado.getContratoLaboral().getId());
            if (contratoLaboralFounded.isPresent() && empleado.getContratoLaboral() != null) {
                personaUpdate.setContratoLaboral(contratoLaboralFounded.get());
            }

            return empleadoRepository.save(personaUpdate);

        }
        return null;
    }

    @Override
    public String eliminar(Long id) {
        try {
            empleadoRepository.deleteById(id);
            return "Eliminacion Correcta";
        } catch (Exception ex) {
            return "Error durante la eliminacion";
        }
    }

    @Override
    public List<Empleado> buscarEmpleado(String texto) {
        return empleadoRepository.buscarNombre(texto);
    }
}
