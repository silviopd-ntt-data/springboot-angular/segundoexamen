package com.example.examen.Service;

import com.example.examen.Entity.Banco;
import com.example.examen.Repository.BancoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BancoServiceImpl implements BancoService {

    @Autowired
    private BancoRepository bancoRepository;

    @Override
    public List<Banco> listar() {
        return bancoRepository.findAll();
    }

    @Override
    public Banco registrar(Banco banco) {
        return bancoRepository.save(banco);
    }

    @Override
    public Banco modificar(Long id, Banco banco) {
        Optional<Banco> bancoFounded = bancoRepository.findById(id);
        if (bancoFounded.isPresent()) {
            Banco personaUpdate = new Banco();
            personaUpdate.setId(id);
            personaUpdate.setNombre(banco.getNombre() != null ? banco.getNombre() : bancoFounded.get().getNombre());
            return bancoRepository.save(personaUpdate);
        }
        return null;
    }

    @Override
    public String eliminar(Long id) {
        try {
            bancoRepository.deleteById(id);
            return "Eliminacion Correcta";
        } catch (Exception ex) {
            return "Error durante la eliminacion";
        }
    }
}
