package com.example.examen.Repository;

import com.example.examen.Entity.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

    @Query(value = "Select * from usuario where active=true", nativeQuery = true)
    List<Usuario> listarUsuarioActivos();

}
