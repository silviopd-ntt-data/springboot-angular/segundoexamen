package com.example.examen.Repository;

import com.example.examen.Entity.ContratoLaboral;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContratoLaboralRepository extends JpaRepository<ContratoLaboral, Long> {
}
