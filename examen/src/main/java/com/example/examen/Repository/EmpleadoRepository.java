package com.example.examen.Repository;

import com.example.examen.Entity.Empleado;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmpleadoRepository extends JpaRepository<Empleado, Long> {

    @Query(value = "select * from empleado where nombres like %?1%", nativeQuery = true)
    public List<Empleado> buscarNombre(String nombre);
}
