package com.example.examen.Controller;

import com.example.examen.Entity.Empleado;
import com.example.examen.Service.EmpleadoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/empleado")
public class EmpleadoController {

    @Autowired
    private EmpleadoService empleadoService;

    @GetMapping("/listar")
    List<Empleado> listar() {
        return empleadoService.listar();
    }

    @PostMapping("/crear")
    Empleado crear(@RequestBody Empleado empleado) {
        return empleadoService.registrar(empleado);
    }

    @PutMapping("/modificar/{id}")
    Empleado modificar(@PathVariable("id") Long id, @RequestBody Empleado empleado) {
        return empleadoService.modificar(id, empleado);
    }

    @DeleteMapping("/eliminar/{id}")
    String eliminar(@PathVariable("id") Long id) {
        return empleadoService.eliminar(id);
    }

    @GetMapping("/buscar")
    public List<Empleado> buscar(@RequestParam("nombre") String texto) {
        return empleadoService.buscarEmpleado(texto);
    }
}

