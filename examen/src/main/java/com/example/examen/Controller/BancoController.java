package com.example.examen.Controller;

import com.example.examen.Entity.Banco;
import com.example.examen.Service.BancoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/banco")
public class BancoController {

    @Autowired
    private BancoService bancoService;

    @GetMapping("/listar")
    List<Banco> listar() {
        return bancoService.listar();
    }

    @PostMapping("/crear")
    Banco crear(@RequestBody Banco banco) {
        return bancoService.registrar(banco);
    }

    @PutMapping("/modificar/{id}")
    Banco modificar(@PathVariable("id") Long id, @RequestBody Banco banco) {
//        System.out.println(id);
        return bancoService.modificar(id, banco);
    }

    @DeleteMapping("/eliminar/{id}")
    String eliminar(@PathVariable("id") Long id) {
        return bancoService.eliminar(id);
    }

}

