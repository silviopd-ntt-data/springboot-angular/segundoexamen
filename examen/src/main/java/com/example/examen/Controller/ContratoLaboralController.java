package com.example.examen.Controller;

import com.example.examen.Entity.ContratoLaboral;
import com.example.examen.Service.ContratoLaboralService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/contratoLaboral")
public class ContratoLaboralController {

    @Autowired
    private ContratoLaboralService contratoLaboralService;

    @GetMapping("/listar")
    List<ContratoLaboral> listar() {
        return contratoLaboralService.listar();
    }

    @PostMapping("/crear")
    ContratoLaboral crear(@RequestBody ContratoLaboral contratoLaboral) {
        return contratoLaboralService.registrar(contratoLaboral);
    }

    @PutMapping("/modificar/{id}")
    ContratoLaboral modificar(@PathVariable("id") Long id, @RequestBody ContratoLaboral contratoLaboral) {
        return contratoLaboralService.modificar(id, contratoLaboral);
    }

    @DeleteMapping("/eliminar/{id}")
    String eliminar(@PathVariable("id") Long id) {
        return contratoLaboralService.eliminar(id);
    }

}

