package com.example.examen.Controller;

import com.example.examen.Entity.Rol;
import com.example.examen.Service.RolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/rol")
public class RolController {

    @Autowired
    private RolService rolService;

    @GetMapping("/listar")
    List<Rol> listar() {
        return rolService.listar();
    }

    @PostMapping("/crear")
    Rol crear(@RequestBody Rol rol) {
        return rolService.registrar(rol);
    }

    @PutMapping("/modificar/{id}")
    Rol modificar(@PathVariable("id") Long id, @RequestBody Rol rol) {
        return rolService.modificar(id, rol);
    }

    @DeleteMapping("/eliminar/{id}")
    String eliminar(@PathVariable("id") Long id) {
        return rolService.eliminar(id);
    }

}

