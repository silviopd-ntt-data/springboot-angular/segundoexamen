package com.example.examen.Controller;

import com.example.examen.Entity.Empleado;
import com.example.examen.Entity.Usuario;
import com.example.examen.Service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/usuario")
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;

    @GetMapping("/listar")
    List<Usuario> listar() {
        return usuarioService.listar();
    }

    @PostMapping("/crear")
    Usuario crear(@RequestBody Usuario usuario) {
        return usuarioService.registrar(usuario);
    }

    @PutMapping("/modificar/{id}")
    Usuario modificar(@PathVariable("id") Long id, @RequestBody Usuario usuario) {
        return usuarioService.modificar(id, usuario);
    }

    @DeleteMapping("/eliminar/{id}")
    String eliminar(@PathVariable("id") Long id) {
        return usuarioService.eliminar(id);
    }

    @GetMapping("/activo")
    public List<Usuario> buscar() {
        return usuarioService.listaEmpleadosActivos();
    }
}

