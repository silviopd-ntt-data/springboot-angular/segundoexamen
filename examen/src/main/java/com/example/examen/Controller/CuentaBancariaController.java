package com.example.examen.Controller;

import com.example.examen.Entity.CuentaBancaria;
import com.example.examen.Service.CuentaBancariaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/cuentaBancaria")
public class CuentaBancariaController {

    @Autowired
    private CuentaBancariaService cuentaBancariaService;

    @GetMapping("/listar")
    List<CuentaBancaria> listar() {
        return cuentaBancariaService.listar();
    }

    @PostMapping("/crear")
    CuentaBancaria crear(@RequestBody CuentaBancaria cuentaBancaria) {
        return cuentaBancariaService.registrar(cuentaBancaria);
    }

    @PutMapping("/modificar/{id}")
    CuentaBancaria modificar(@PathVariable("id") Long id, @RequestBody CuentaBancaria cuentaBancaria) {
        return cuentaBancariaService.modificar(id, cuentaBancaria);
    }

    @DeleteMapping("/eliminar/{id}")
    String eliminar(@PathVariable("id") Long id) {
        return cuentaBancariaService.eliminar(id);
    }

}

