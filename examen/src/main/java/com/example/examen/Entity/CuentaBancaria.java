package com.example.examen.Entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "cuentaBancaria")
public class CuentaBancaria implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nummero_cuenta")
    private String nroCuenta;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_banco")
    @JsonIgnoreProperties("cuentaBancariaList")
    private Banco banco;

    @OneToOne(mappedBy = "cuentaBancaria", cascade = CascadeType.ALL)
    @JsonIgnoreProperties("cuentaBancaria")
    private Empleado empleado;

}
