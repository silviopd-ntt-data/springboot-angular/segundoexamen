package com.example.examen.Entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "contratoLaboral")
public class ContratoLaboral implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "sueldo_base")
    private double sueldoBase;

    @Column(name = "tipo_contrato")
    private double tipoContrato;

    @OneToMany(mappedBy = "contratoLaboral", cascade = CascadeType.ALL)
    @JsonIgnoreProperties("contratoLaboral")
    private List<Empleado> empleadoList;
}
