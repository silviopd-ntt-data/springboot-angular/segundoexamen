package com.example.examen.Entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "empleado")
public class Empleado implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String nombres;
    private String apellidos;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_cuenta")
    @JsonIgnoreProperties("empleado")
    private CuentaBancaria cuentaBancaria;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_contrato")
    @JsonIgnoreProperties("empleadoList")
    private ContratoLaboral contratoLaboral;

    @OneToOne(mappedBy = "empleado", cascade = CascadeType.ALL)
    @JsonIgnoreProperties("empleado")
    private Usuario usuario;
}
